# Phenomine Subset
The whole dataset is based around the plot unique ID which is a unique name for each plot of wheat in a given year, for example:

    FPWW0220001

The 11 digits are composed as such:

    FP[YEARLY EXPERIMENT NUMBER][PLOT NUMBER]

Where the yearly experiment number for 2018 is WW022 (Winter Weizen) and the plot number is usually between 0 and 800.    

- All images and all the traits references this number (aside for the weather data FIP_WEATHER). 
- For all pictures a timestamp in the format of YYYYMMDD_HHMMSS is appended. 

Target traits and covariate data are extracted directly from the CroPyDB database: [docs/CroPyDB_documentation](docs/CroPyDB_documentation.pdf), a file for each Lot (set of 400 plots) is generated and the relevant data is in the "value" column. In case of timeseries a "timestamp" column is also present.

Example of traits filenames:

    - trait_data_FPWW022_lot3_trait_id_8_method_id_352.csv
    - trait_data_FPWW022_lot1_trait_id_8_method_id_352.csv

The naming of the traits files reflects the database internal IDs and each trait is represented by a trait_id and a method_id, with the following mapping:

| Trait                  | trait_id | method_id |
|------------------------|----------|-----------|
| Yield                  | 8        | 352       |
| Protein Content        | 9        | 7         |
| Thousand Kernel Weight | 10       | 6         |
| Infestation Take All   | 27       | 47        |
| Winter Kill Rate       | 31       | 51        |

For the covariate datasets EXP_GS, EXP_PH and EXP_WEATHER only a single trait is present and the mapping is therefore not necessary.

More information about each dataset: [Phenomine Datasets Description](README.md)


## Filepaths
```
/data/shared/raw_data/phenomine-subset/
├── /FIP_RGB/ 
│   └── /2018/
│       ├── FPWW0220001_RGB1_20171213_140724.CR2
│       ├── FPWW0220001_RGB1_20180112_120402.CR2
│       └── ... (21884 pictures not aligned)
├── /FIP_PLOT/
│   └── /2018/
│       ├── /ALIGN_REFERENCES/
│       │   ├── FPWW0220001_RGB1_20171213_140724.geojson
│       │   ├── FPWW0220001_RGB1_20171213_140724.geojson
│       │   └── ... (17678 geojson files with plot polygons)
│       ├── /PLOT_SHAPES/
│       │   ├── FPWW0220031.geojson
│       │   ├── FPWW0220072.geojson
│       │   └── ... (61 geojson files with wheat rows polygons)
│       └── /PLOTS/
│           ├── FPWW0220001_RGB1_20171213_140724.CR2
│           ├── FPWW0220001_RGB1_20180112_120402.CR2
│           └── ... (1514 pictures of 7 inner wheat rows)
├── /EXP_TRAIT/
│   ├── trait_data_FPWW022_lot1_trait_id_10_method_id_6.csv
│   ├── trait_data_FPWW022_lot1_trait_id_9_method_id_7.csv
│   └── ... (10 .csv files with target traits)
├── /EXP_PH/
│   ├── trait_data_FPWW022_lot1_trait_id_5_method_id_304.csv
│   └── trait_data_FPWW022_lot3_trait_id_5_method_id_304.csv
├── /EXP_GS/
│   ├── trait_data_FPWW022_lot1_trait_id_21_method_id_42.csv
│   └── trait_data_FPWW022_lot3_trait_id_21_method_id_42.csv
├── /FIP_DESIGN/2018
│   ├── design_FPWW022_lot1.csv
│   ├── design_FPWW022_lot3.csv
│   └── genotypes.csv
├── /FIP_WEATHER/
│   └── covar_data_position_id_1_year_range_2016-2022.csv
└── /FIP_SEGMENT/
    └── /2018/
        ├── FPWW0220477_20180316.tif
        └── ... (1495 segmentations of FIP_RGB) 
```

## /FIP_DESIGN/2018
This folder contains the mapping of the plot unique id (plot.UID) with the genotype id aswell as the mapping from genotype id to the genotype name (and information about the breeder.)


## /FIP_PLOT/2018

This folder contains the information about both the location of the plot and the rows of wheat in the images of FIP_RGB aswell as the extracted images.

## /ALIGN_REFERENCES
Contains a geojson file for each image in FIP_RGB with the location of the plot as a polygon.

### /PLOT_SHAPES
Contains a geojson file for each plot with the location of the rows of wheat (referenced to the plot if ALIGN_REFERENCES)

### /PLOTS
Contains the cropped out plots to the inner 7 rows of wheat (discarding the edge rows). It is generated using both /ALIGN_REFERENCES and /PLOT_SHAPES through the script [plot_extraction.py](tools/plot_extraction.py).




