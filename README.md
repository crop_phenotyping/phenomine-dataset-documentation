# Pheno-Mine Datasets
## FIP_RGB
| *.CR2 (Canon raw) | IMAGE |
|-------------------|-------|

![](examples/FIP_RGB.gif)

- Each image shows an outdoor winter wheat plot (a uniform plant canopy) in nadir view (3 m distance) at a specific measurement day. Plots are part of an experimental design that involves replications in space and time.
- 800 plots were measured in 7 years (2015-2021) approximately twice a week for the full growing season. Exposure settings were adjusted on a measurement day base using intensity histograms. A measurement day took approximately 2-3 h.
- The FIP_RGB dataset forms the base of this project and will become annotate with additional datasets.
- Number of images: ~400’000
- Image size: 5636 x 3745 pixels
- Ground sampling distance: 0.3 mm/pixel. Comparisons with objects of interest: (a) wheat leaf in spring: ~3 mm x 10 mm, (b) wheat leaf in summer: ~30 mm x 50 mm, (c) whole plant canopy: 1000 mm x 1500 mm
- Labelling the data is done using the existing datasets EXP_PH, EXP_GS, EXP_TRAIT and the datasets that are in progress, FIP_SEGMENT and FIP_PLOT. To standardize this technical labeling step and to allow future open access publication of FIP_RGB is part of the project.
- Plot identification and orthorectification of the dataset using the FIP_PLOT dataset is already done
- Preprocessing of 14-bit RAW images to a standard image format may be required
- Differing illumination conditions and irregular measurement intervals may present challenges in the further processing steps.

## FIP_PLOT
| Numeric | DATA ARRAY |
|---------|------------|

![](examples/FIP_PLOT.gif)

- The dataset describes the camera pose and plot location of each individual image
- This data is needed to annotate plots and to train an alternative image alignment algorithm 
- 1 pose estimation and 1 plot polygon per record
- Records: Corresponding to the number of images: ~400’000
- No labelling is required
- The preprocessing is ongoing and partly done (1 of 4 years)
- An alternative approach may be used to preprocess the other years based on trainings on the already preprocessed data. 
- As the annotation process was supervised, biases and uncertainties are minimized.
- Reducing the amount of missing data (75%, 3 years) is ongoing

### FIP_PLOT pre-processing steps

#### Plot alignment
![](examples/FIP_PLOT--alignment.gif)
#### Rows alignment
![](examples/FIP_PLOT--rows.png)


## FIP_SEGMENT
| TIFF | IMAGE |
|------|-------|

![](examples/FIP_SEGMENT.gif)

- Segmented images of wheat plots showing plant and soil pixels
- Same dimensions, size and ground sampling distance as FIP_RGB 
- The dataset may serve to label zones of interest (plants) in images
- Preprocessing included the training of a semantic segmentation (CNN) and applying the winning model to all images, publication of the method is in progress
- No further preprocessing is needed
- Differing illumination conditions may systematically bias the segmentation process

## EXP_PH
| Numeric | TIME SERIES |
|---------|-------------|

![](examples/canopy_heights.png)
- These time series capture the height development of canopies
- The data was captured using a terrestrial laser scanner and drone-based structure-from-motion
- Canopy heights are needed to annotate the correct position of plots in images
- Number of times series: 7 x 800 plots
- Lengths of time series: 15 - 20 data points
- The sampling frequency is high in comparison to targeted events, e.g., end of stem elongation
- The dataset is already labelled
- RAW measurements are already processed to canopy heights
- No further preprocessing is required
- Missing data is very rare (<1 %) and at random
- The data is expected to change over time, repetition and sensor, but corresponding corrections with references are already included in the preprocessing


## EXP_GS
| Categorical | DATA ARRAY |
|-------------|------------|
- This data array describes the development of plant canopies using growth stage (GS) annotations, e.g., start of stem elongation, start of heading, and end of heading time points
- The measurement protocol was based on the BBCH scale, a widely accepted standard scale to rate the phenological development stages of plants
- This dataset is needed to annotate images
- Record on every plot (7 years x 800 plots)
- A record consists of >= 1 growth stages (start of stem elongation, heading, senescence)
- The dataset is already labelled
- RAW field measurements are already processed to growth stages
- No further preprocessing is required
- The data is not biased and corresponds to the gold standard of annotating growth stages by hand
- Depending on the year, systematically missing data may be as high as 50% (if e.g. only one replication was rated)

## EXP_TRAIT
| Numeric | DATA ARRAY |
|---------|------------|
- This dataset describes specific manual ratings, e.g., disease ratings, stress indicators, protein content, harvest weights
- The data serves as target variable
- 1 to ~ 5 entries per record
- 1 record per plot, therefore 7 x 800
- No labelling is required
- RAW field measurements are already processed to traits
- No further preprocessing is required
- Uncertainties on the measurements may be very high due to the small plot sizes. Nevertheless, these uncertainties represent an unalterable restriction of field experiments and therefore define the limits of this real-world dataset.
- Missing data is very rare (<1 %) and at random

## FIP_WEATHER
| Numeric | TIME SERIES |
|---------|-------------|
- This dataset describes the environmental conditions at the FIP site
- The data was captured using a local weather station with several dedicated sensors (air temperature and humidity, soil temperature and humidity, wind direction and speed, radiance, and precipitation)
- This dataset may serve as essential component to characterize growing seasons
- Number of times series: 7 (years)
- Lengths of time series: Data point every 10 minutes, therefore 52,560 data points per serie / year
- The sampling frequency is high in comparison to targeted events, e.g., end of stem elongation in days versus temperature sampling every 10 minutes
- The dataset is already labeled
- RAW measurements are already processed to meteorological variables
- No further preprocessing is required
- Missing data is very rare (<1 %) but not at random (serially correlated gaps)
- No systematic bias is known
