import os

import rawpy
import imageio
import json
import numpy as np
from PIL import Image, ImageDraw
import cv2
import argparse


my_parser = argparse.ArgumentParser()
my_parser.add_argument('--segmentedImgsDir', action='store', type=str, required=True)
my_parser.add_argument('--plotGeojsonDir', action='store', type=str, required=True)
my_parser.add_argument('--rowGeojsonDir', action='store', type=str, required=True)
my_parser.add_argument('--outputDir', action='store', type=str, required=True)
args = my_parser.parse_args()

segmentedImgsDir = args.segmentedImgsDir
plot_geojson_dir = args.plotGeojsonDir
row_geojson_dir = args.rowGeojsonDir

output_dir = args.outputDir


# Iterate all RGB images
for raw_img_filename in os.listdir(segmentedImgsDir):
    print("Cropping image " + raw_img_filename)
    raw_img_path = os.path.join(segmentedImgsDir,raw_img_filename)

    # Check if images is at the expected location.
    if not os.path.exists(raw_img_path):
        print("Image not at expected location.")
        continue

    # Retrieve path of plot alignment file
    plotuid = raw_img_filename[:11]
    row_geojson_filename = plotuid + ".geojson"
    row_geojson_path = os.path.join(row_geojson_dir,plotuid + ".geojson")
    if not os.path.exists(row_geojson_path):
        print("Couldn't find plot alignment file, skip image.")
        print(row_geojson_path)
        continue

    # Retrieve path of plot shape file
    plot_geojson_filename = raw_img_filename[:-9] + ".geojson"
    plot_geojson_path = os.path.join(plot_geojson_dir, plot_geojson_filename)
    if not os.path.exists(plot_geojson_path):
        print("Couldn't find plot shape file, skip image.")
        print(plot_geojson_path)
        continue

    # *******
    # Crop image
    test_img_path = raw_img_path
    rows_geojson_path = row_geojson_path

    # ***************
    # Load RAW image
    # ***************

    rgb = imageio.imread(str(test_img_path))

    print("Raw image loaded.")

    # ***************
    # Load geojson file with plot outline polygon
    # Generate mask to remove pixel outside the plot
    # ***************

    plot_geojson_file = open(plot_geojson_path, "r")
    plot_geojson = json.loads(plot_geojson_file.read())
    plot_polygon = plot_geojson["features"][0]["geometry"]["coordinates"]

    # Transform polygon in correct format.
    int_plot_geojson = []
    for vertex_idx in range(len(plot_polygon[0])):
        int_plot_geojson.append((int(plot_polygon[0][vertex_idx][0]),int(plot_polygon[0][vertex_idx][1])))

    # Generate mask to remove pixels outside plot
    plot_mask_img = Image.new('L', (rgb.shape[1], rgb.shape[0]), 0)
    ImageDraw.Draw(plot_mask_img).polygon(int_plot_geojson, outline=0, fill=1)
    plot_mask_np = np.array(plot_mask_img)

    # Mask pixels outside the plot
    rgb[:,:] = rgb[:,:]*plot_mask_np
    # rgb[:,:,1] = rgb[:,:,1]*plot_mask_np
    # rgb[:,:,2] = rgb[:,:,2]*plot_mask_np

    # ***************
    # Load geojson with plot-shape (alignment to 7 inner rows out of the 9 rows of wheat)
    # ***************

    rows_geojson_file = open(rows_geojson_path, "r")
    rows_geojson = json.loads(rows_geojson_file.read())
    rows_polygon = rows_geojson["features"][0]["geometry"]["coordinates"]
    int_rows_geojson = []
    for vertex_idx in range(len(plot_polygon[0])):
        int_rows_geojson.append((int(rows_polygon[0][vertex_idx][0]),int(rows_polygon[0][vertex_idx][1])))

    img = Image.new('L', (rgb.shape[0], rgb.shape[1]), 0)
    ImageDraw.Draw(img).polygon(int_rows_geojson, outline=0, fill=1)
    rows_mask = np.array(img)

    # Do perspective transformation to retrieve plot shape in raw image
    # Perspective transformation of plot shape on the full img

    # Image plot outline
    pts1 = np.float32([[0,0],[0,0],[0,0],[0,0]])
    pts1[1] = [0,0]
    pts1[0] = [rows_mask.shape[1],0]
    pts1[3] = [rows_mask.shape[1], rows_mask.shape[0]]
    pts1[2] = [0,rows_mask.shape[0]]

    pts2 = np.float32(plot_polygon[0][:4])

    M = cv2.getPerspectiveTransform(pts1,pts2)
    dst = cv2.warpPerspective(rows_mask, M, rows_mask.shape)


    # Mask pixels outside the plot
    rgb[:,:] = rgb[:,:]*dst
    # rgb[:,:,1] = rgb[:,:,1]*dst
    # rgb[:,:,2] = rgb[:,:,2]*dst


    # Compute location of plot shape vertices
    pts3 = []
    for i in range(4):
        p = (int_rows_geojson[i][0],int_rows_geojson[i][1]) # your original point
        matrix = M
        px = (matrix[0][0]*p[0] + matrix[0][1]*p[1] + matrix[0][2]) / ((matrix[2][0]*p[0] + matrix[2][1]*p[1] + matrix[2][2]))
        py = (matrix[1][0]*p[0] + matrix[1][1]*p[1] + matrix[1][2]) / ((matrix[2][0]*p[0] + matrix[2][1]*p[1] + matrix[2][2]))
        p_after = (int(px), int(py)) # after transformation
        print(p_after)
        rgb[int(py)-5:int(py)+5,int(px)-5:int(px)+5] = 255
        pts3.append([px,py])

    pts3 = np.float32(pts3)


    # Warp plot to full image size
    pts1[3] = [0,0]
    pts1[0] = [rows_mask.shape[0],0]
    pts1[1] = [rows_mask.shape[0], rows_mask.shape[1]]
    pts1[2] = [0,rows_mask.shape[1]]

    M = cv2.getPerspectiveTransform(pts3,pts1)
    final = cv2.warpPerspective(rgb, M, rows_mask.shape)

    imageio.imsave(os.path.join(output_dir,raw_img_filename[:-4] + ".tiff"), final)
